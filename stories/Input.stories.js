import React from 'react';

import {storiesOf} from '@storybook/react';
import {Input, Label} from '../src/components/Input';
import SelectField from '../src/components/SelectField';

const storyOfInput = () =>
  storiesOf('Input', module)
    .add('default', () => (
      <>
        <Label>Input</Label>
        <Input name="input" />
      </>
    ))
    .add('dropdown', () => (
      <>
        <Label>Dropdown</Label>
        <SelectField
          options={[
            {value: 'Food', label: 'Food'},
            {value: 'Travel', label: 'Travel'},
            {value: 'Shop', label: 'Shop'},
            {value: 'Health', label: 'Health'},
          ]}
          // eslint-disable-next-line no-console
          onChange={option => console.log(option)}
        />
      </>
    ));

export default storyOfInput;
