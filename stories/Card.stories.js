import React from 'react';

import {storiesOf} from '@storybook/react';
import Card from '../src/components/Card';

const storyOfCard = () =>
  storiesOf('Card', module).add('default', () => (
    <>
      <Card>
        <h1>This is card</h1>
      </Card>
      <Card fluid>
        <h1>This is card fluid</h1>
      </Card>
    </>
  ));

export default storyOfCard;
