import storyOfText from './Text.stories';
import storyOfButton from './Button.stories';
import storyOfCard from './Card.stories';
import storyOfInput from './Input.stories';

storyOfText();
storyOfButton();
storyOfCard();
storyOfInput();
