import React from 'react';

import {storiesOf} from '@storybook/react';
import Text from '../src/components/Text';

const storyOfText = () =>
  storiesOf('Text', module)
    .add('default', () => (
      <>
        <Text size="very-small">Hello</Text>
        <Text size="small">Hello</Text>
        <Text size="medium">Hello</Text>
        <Text size="large">Hello</Text>
        <Text size="very-large">Hello</Text>
      </>
    ))
    .add('with color', () => (
      <>
        <Text color="red">Hello</Text>
        <Text color="green">Hello</Text>
        <Text color="blue">Hello</Text>
        <Text color="purple">Hello</Text>
      </>
    ));
export default storyOfText;
