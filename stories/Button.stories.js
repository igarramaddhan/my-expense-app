import React from 'react';

import {storiesOf} from '@storybook/react';
import {action} from '@storybook/addon-actions';
import Button from '../src/components/Button';
import FloatingActionButton from '../src/components/FloatingActionButton';
import {Plus} from 'react-feather';
import COLORS from '../src/lib/colors';

const storyOfButton = () =>
  storiesOf('Button', module)
    .add('default', () => (
      <>
        <code>default</code>
        <Button onClick={action('clicked')}>Button</Button>
        <code>fluid</code>
        <Button fluid onClick={action('clicked')}>
          Button
        </Button>
      </>
    ))
    .add('type', () => (
      <>
        <code>submit</code>
        <Button type="submit" onClick={action('clicked')}>
          Button
        </Button>
        <code>cancel</code>
        <Button type="cancel" onClick={action('clicked')}>
          Button
        </Button>
      </>
    ))
    .add('floating action', () => (
      <>
        <FloatingActionButton icon={<Plus onClick={action('clicked')} />} />
        <FloatingActionButton
          style={{backgroundColor: COLORS.BLUE}}
          icon={<Plus color="white" onClick={action('clicked')} />}
        />
        <FloatingActionButton
          style={{backgroundColor: COLORS.RED}}
          icon={<Plus color="white" onClick={action('clicked')} />}
        />
      </>
    ));

export default storyOfButton;
