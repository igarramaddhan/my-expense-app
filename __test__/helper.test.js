import {
  isDarkColor,
  convertToRupiah,
  getValueFromObject,
  getDateFormatString,
} from '../src/lib/helper';

describe('Helper Testing: isDarkColor', () => {
  test('It return true if color is #123061: ', () => {
    const color = '#123061';
    expect(isDarkColor(color)).toBe(true);
  });

  test('It return false if color is #5b8bd9: ', () => {
    const color = '#5b8bd9';
    expect(isDarkColor(color)).toBe(false);
  });
});

describe('Helper Testing: convertToRupiah', () => {
  test('It return Rp. 20.000 from 20000: ', () => {
    const price = 20000;
    expect(convertToRupiah(price)).toBe('Rp. 20.000');
  });
});

describe('Helper Testing: getValueFromObject', () => {
  test('It return the object value: ', () => {
    const obj = {
      a: {
        b: {
          c: 'This is the value',
        },
      },
    };

    expect(getValueFromObject(obj, 'a.b.c')).toBe('This is the value');
  });

  test('It return undefined value: ', () => {
    const obj = {
      a: {
        b: {},
      },
    };

    expect(getValueFromObject(obj, 'a.b.c')).toBe(undefined);
  });
});

describe('Helper Testing: getDateFormatString', () => {
  test('It return DD-MM-YYYY if DAY is passed: ', () => {
    expect(getDateFormatString('DAY')).toBe('DD-MM-YYYY');
  });
});
