// @flow
import React from 'react';
import styled from 'styled-components';
import Scaffold from '../components/Scaffold';
import COLORS from '../lib/colors';
import Tab from '../components/Tab';
import {withExpenses} from '../contexts/ExpenseContext';
import {Plus} from 'react-feather';
import FloatingActionButton from '../components/FloatingActionButton';
import {ModalContext} from '../contexts/ModalContext';
import ExpenseList from '../components/ExpenseList';
import AddExpenseForm from '../components/AddExpenseForm';

const Container = styled.div`
  flex: 1;
  display: flex;
  flex-direction: column;
  color: white;
  box-sizing: border-box;

  h2.welcome {
    font-size: 32pt;
  }

  p.instruction {
    color: white;
  }
`;

const ContentContainer = styled.div`
  display: flex;
  padding: 24px;
  box-sizing: border-box;
  flex-direction: column;
  min-height: calc(100vh * 40 / 100);
`;

const tabs = ['Day', 'Month', 'Year'];

const Index = (props: any) => {
  const [state, setState] = React.useState('Day');
  const modalCtx = React.useContext(ModalContext);

  return (
    <Scaffold
      headerTextColor="#ffff"
      headerText="My expenses"
      headerColor={COLORS.LIGHT_BLUE}
      extraHeaderContent={
        <Tab
          items={tabs}
          backgroundColor={COLORS.BLUE}
          active={tabs.indexOf(state)}
          itemOptions={{
            activeColor: 'white',
            activeTextColor: COLORS.BLUE,
            inactiveColor: COLORS.BLUE,
            inactiveTextColor: COLORS.DEFAULT_TEXT,
          }}
          onItemClick={idx => setState(tabs[idx])}
        />
      }>
      <AddExpenseForm />
      <Container>
        <div
          style={{
            right: 16,
            bottom: 16,
            position: 'absolute',
          }}>
          <FloatingActionButton
            icon={<Plus color="white" onClick={() => modalCtx.toggleModal()} />}
            style={{
              bottom: '16px',
              position: 'fixed',
              transform: 'translateX(-120%)',
              backgroundColor: COLORS.LIGHT_BLUE,
            }}
          />
        </div>
        <ContentContainer>
          <ExpenseList
            groupBy={state.toUpperCase()}
            items={props.getExpenses(state.toUpperCase())}
          />
        </ContentContainer>
      </Container>
    </Scaffold>
  );
};

export default withExpenses(Index);
