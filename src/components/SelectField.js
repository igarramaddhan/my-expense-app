// @flow
import React from 'react';
import {FieldProps} from 'formik';
// $FlowFixMe
import Select, {ReactSelectProps, Option} from 'react-select';

const SelectField = ({
  options,
  field,
  form,
  ...props
}: ReactSelectProps & FieldProps) => {
  const prop = field ? field : props;
  return (
    <Select
      options={options}
      name={prop.name}
      value={
        options ? options.find(option => option.value === prop.value) : null
      }
      onChange={(option: Option) =>
        field
          ? form.setFieldValue(prop.name, option.value)
          : props.onChange(option)
      }
      onBlur={prop.onBlur}
      styles={{
        container: provided => ({...provided, marginTop: '4px'}),
      }}
    />
  );
};

export default SelectField;
