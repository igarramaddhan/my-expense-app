// @flow
import React from 'react';
import styled from 'styled-components';

const sizes = {
  small: 40,
  medium: 48,
  large: 56,
};

const FloatingActionButtonContainer = styled.button`
  margin: 4px;
  border: none;
  display: flex;
  outline: none;
  cursor: pointer;
  align-items: center;
  justify-content: center;
  width: ${props => sizes[props.size]}px;
  height: ${props => sizes[props.size]}px;
  border-radius: ${props => sizes[props.size] / 2}px;
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.3), 0 3px 6px rgba(0, 0, 0, 0.3);
`;

type Props = {
  icon: any,
  // $FlowFixMe
  style?: React.CSSProperties,
  size?: 'small' | 'medium' | 'large',
};

const FloatingActionButton = ({size = 'medium', icon, ...props}: Props) => (
  <FloatingActionButtonContainer size={size} {...props}>
    {icon}
  </FloatingActionButtonContainer>
);

export default FloatingActionButton;
