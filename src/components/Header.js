// @flow
import React from 'react';
import styled from 'styled-components';
import {isDarkColor} from '../lib/helper';

const HeaderContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  z-index: 10;
  min-height: 56px;
  display: flex;
  position: fixed;
  flex-direction: row;
  justify-content: center;
`;

const HeaderContentContainer = styled.div`
  flex: 1;
  display: flex;
  max-width: 480px;
  flex-direction: column;
  justify-content: center;
  background-color: ${props =>
    props.backgroundColor ? props.backgroundColor : 'white'};
  color: ${props =>
    props.textColor
      ? props.textColor
      : isDarkColor(props.backgroundColor)
      ? 'white'
      : 'black'};
`;

const Title = styled.h2`
  margin: 0;
  height: 56px;
  display: flex;
  align-self: center;
  align-items: center;
`;

type Props = {
  text?: string,
  children?: any,
  textColor?: string,
  backgroundColor?: string | '#ffff',
};

const Header = (props: Props, ref) => (
  <HeaderContainer ref={ref}>
    <HeaderContentContainer
      textColor={props.textColor}
      backgroundColor={props.backgroundColor}>
      <Title>{props.text}</Title>
      {props.children}
    </HeaderContentContainer>
  </HeaderContainer>
);

export default React.forwardRef<Props, any>(Header);
