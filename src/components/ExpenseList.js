// @flow
import React from 'react';
import styled from 'styled-components';
import {type Expense} from '../contexts/ExpenseContext';
import {Calendar} from 'react-feather';
import moment from 'moment';
import {getDateFormatString} from '../lib/helper';
import ExpenseItem from './ExpenseItem';
import Text from './Text';

const SectionText = styled(Text)`
  display: flex;
  margin-top: 16px;
  margin-left: 16px;
  margin-bottom: 10px;
  font-weight: bold;
  align-items: center;
`;

const VerticalLine = styled.div`
  height: 16px;
  width: 27px;
  margin: -8px 0;
  border-right: 2px dashed #d2d6e4;
`;

const EmptyContainer = styled.div`
  flex: 1;
  color: black;
  display: flex;
  align-items: center;
  justify-content: center;
`;

const sectionFormat = {
  DAY: 'MMMM DD, YYYY',
  MONTH: 'MMMM, YYYY',
  YEAR: 'YYYY',
};

type Props = {
  items: {
    day: string,
    expenses: Expense[],
  }[],
  groupBy: string | 'DAY' | 'MONTH' | 'YEAR',
};

const ExpenseList = ({items, groupBy}: Props) => (
  <>
    {items.length !== 0 ? (
      items.map((val, idx) => (
        <React.Fragment key={idx}>
          <SectionText color="#D2D6E4">
            <Calendar style={{marginRight: '8px'}} />
            {moment(val.day, getDateFormatString(groupBy))
              .format(sectionFormat[groupBy])
              .toUpperCase()}
          </SectionText>
          {val.expenses.map(val => (
            <React.Fragment key={val.id}>
              <VerticalLine />
              <ExpenseItem expense={val} />
            </React.Fragment>
          ))}
        </React.Fragment>
      ))
    ) : (
      <EmptyContainer>No Items</EmptyContainer>
    )}
  </>
);

export default ExpenseList;
