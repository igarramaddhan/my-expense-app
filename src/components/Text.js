// @flow
import React from 'react';
import styled from 'styled-components';

const StyledText = styled.p`
  margin: 2px;
  color: ${props => (props.color ? props.color : '#A5AABB')};
  font-size: ${props => {
    let size = '';
    switch (props.size) {
      case 'very-small':
        size = '8px';
        break;
      case 'small':
        size = '11px';
        break;
      case 'medium':
        size = '16px';
        break;
      case 'large':
        size = '24px';
        break;
      case 'very-large':
        size = '32px';
        break;
      default:
        size = '16px';
        break;
    }
    return size;
  }};
`;

type Props = {
  children: any,
  color?: string,
  // $FlowFixMe
  style?: React.CSSProperties,
  as?: 'h1' | 'h2' | 'h3' | 'h4' | 'h5' | 'h6' | 'p',
  size?: 'very-small' | 'small' | 'medium' | 'large' | 'very-large',
};
const Text = ({children, as = 'p', ...props}: Props) => (
  <StyledText as={as} {...props}>
    {children}
  </StyledText>
);

export default Text;
