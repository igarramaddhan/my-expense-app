// @flow
import React from 'react';
import styled from 'styled-components';

const CardContainer = styled.div`
  padding: 8px;
  margin: 8px 0;
  min-height: 100px;
  background: white;
  border-radius: 8px;
  flex-direction: column;
  box-sizing: border-box;
  width: ${props => (props.fluid ? '100%' : 'unset')};
  display: ${props => (props.fluid ? 'flex' : 'inline-block')};
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.05), 0 3px 6px rgba(0, 0, 0, 0.05);
`;

type Props = {
  children: any,
  fluid?: boolean,
  // $FlowFixMe
  style?: React.CSSProperties,
};

const Card = ({children, fluid = false, ...props}: Props) => (
  <CardContainer fluid={fluid} {...props}>
    {children}
  </CardContainer>
);

export default Card;
