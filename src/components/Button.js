// @flow
import React from 'react';
import styled from 'styled-components';
import COLORS from '../lib/colors';

const BUTTON_COLOR = {
  cancel: COLORS.RED,
  submit: COLORS.GREEN,
};

const ButtonStyled = styled.button`
  outline: none;
  display: flex;
  margin: 4px 0;
  border-width: 0;
  font-size: 16px;
  min-height: 38px;
  padding: 2px 8px;
  font-weight: bold;
  position: relative;
  border-radius: 2px;
  align-items: center;
  justify-content: center;
  text-transform: uppercase;
  transition: background-color 0.3s;
  width: ${props => (props.fluid ? '100%' : 'unset')};
  background-color: ${props =>
    props.type ? BUTTON_COLOR[props.type] : '#eee'};
  box-shadow: 0 3px 6px rgba(0, 0, 0, 0.1), 0 3px 6px rgba(0, 0, 0, 0.1);
  color: ${props =>
    props.type === 'submit' || props.type === 'cancel' ? 'white' : 'black'};
`;

// $FlowFixMe
type Props = React.HTMLProps<HTMLButtonElement> & {
  fluid?: boolean,
};

const Button = ({fluid = false, ...props}: Props) => (
  <ButtonStyled fluid={fluid} {...props}>
    {props.children}
  </ButtonStyled>
);

export default Button;
