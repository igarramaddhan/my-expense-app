// @flow
import React from 'react';
import styled from 'styled-components';
import Card from './Card';
import ExpenseIcon from './ExpenseIcon';
import {type Expense, ExpenseContext} from '../contexts/ExpenseContext';
import {convertToRupiah} from '../lib/helper';
import Text from './Text';
import COLORS from '../lib/colors';
import {Trash2} from 'react-feather';

const CardContent = styled.div`
  display: flex;
  margin-left: 8px;
  flex-direction: column;
`;

const TypeContainer = styled.div`
  flex: 1;
  display: flex;
  margin-right: 16px;
  align-items: center;
  justify-content: flex-end;
`;

type Props = {
  expense: Expense,
};

const ExpenseItem = ({expense}: Props) => {
  const ctx = React.useContext(ExpenseContext);
  return (
    <Card fluid style={{flexDirection: 'row'}}>
      <ExpenseIcon
        category={expense.category}
        containerStyle={{margin: '4px'}}
      />
      <CardContent>
        <Text as="h1" size="large">
          {convertToRupiah(expense.value)}
        </Text>
        <Text>{expense.title}</Text>
      </CardContent>
      <TypeContainer>
        <Text color={expense.type === 'SPENDING' ? COLORS.RED : COLORS.GREEN}>
          {expense.type}
        </Text>
        <Trash2
          color="red"
          size={25}
          style={{marginLeft: '10%', cursor: 'pointer'}}
          onClick={() => ctx.deleteExpense(expense.id)}
        />
      </TypeContainer>
    </Card>
  );
};

export default ExpenseItem;
