// @flow
import React from 'react';
import styled from 'styled-components';
import {FieldProps} from 'formik';

export const Label = styled.p`
  color: grey;
  margin-top: 8px;
  margin-bottom: 0;
`;

const InputStyled = styled.input`
  width: 100%;
  display: flex;
  flex-wrap: wrap;
  font-size: 16px;
  margin-top: 4px;
  min-height: 38px;
  padding: 2px 8px;
  border-width: 1px;
  position: relative;
  border-radius: 4px;
  border-style: solid;
  outline: 0 !important;
  box-sizing: border-box;
  border-color: hsl(0, 0%, 80%);
  background-color: hsl(0, 0%, 100%);
`;

type Props = FieldProps;

export const Input = ({field, ...props}: Props) => (
  <InputStyled {...field} {...props} />
);
