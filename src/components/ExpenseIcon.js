// @flow
import React from 'react';
import styled from 'styled-components';
import {
  Box,
  Heart,
  Smile,
  Briefcase,
  CreditCard,
  ShoppingBag,
} from 'react-feather';
import {type ExpenseCategory} from '../contexts/ExpenseContext';

const ExpenseIconContainer = styled.div`
  width: 20px;
  height: 20px;
  padding: 5px;
  display: flex;
  border-radius: 8px;
  align-items: center;
  justify-content: center;
  background-color: ${props => props.backgroundColor};
`;

const icons = {
  Food: {icon: Smile, color: '#96d88d'},
  Travel: {icon: Briefcase, color: '#f3aba8'},
  Shop: {icon: ShoppingBag, color: '#f8ce46'},
  Health: {icon: Heart, color: '#ec818f'},
  Other: {icon: Box, color: '#061d42'},
  Bank: {icon: CreditCard, color: '#000000'},
};

type Props = {
  category: ExpenseCategory,
  // $FlowFixMe
  containerStyle?: React.CSSProperties,
};

const ExpenseIcon = ({category, containerStyle, ...props}: Props) => {
  const Icon = icons[category].icon;
  return (
    <ExpenseIconContainer
      {...props}
      style={containerStyle}
      backgroundColor={icons[category].color}>
      <Icon color="white" />
    </ExpenseIconContainer>
  );
};

export default ExpenseIcon;
