// @flow
import React from 'react';
import * as Yup from 'yup';
import moment from 'moment';
import styled from 'styled-components';
import {Formik, Form, ErrorMessage, Field} from 'formik';
import COLORS from '../lib/colors';
import Text from './Text';
import Button from './Button';
import {ModalContext} from '../contexts/ModalContext';
import {ExpenseContext} from '../contexts/ExpenseContext';
import Modal from './Modal';
import SelectField from './SelectField';
import {Input, Label} from './Input';

const FormContainer = styled.div`
  flex: 1;
  padding: 16px;
  background-color: ${COLORS.BACKGROUND};
`;

const ErrorMessageStyled = styled.div`
  margin: 0 8px;
  color: #ff2b4f;
  font-size: 14px;
`;

const DateContainer = styled.div`
  display: flex;
`;

const DateContentContainer = styled.div`
  display: flex;
  flex-direction: column;
`;

const ButtonContainer = styled.div`
  display: flex;
  margin: 8px 0;
`;

const moreThanZeroValidator = Yup.number()
  .moreThan(0)
  .required('Required');

const AddExpenseSchema = Yup.object().shape({
  day: moreThanZeroValidator,
  month: moreThanZeroValidator,
  year: moreThanZeroValidator,
  title: Yup.string().required('Required'),
  category: Yup.string()
    .oneOf(['Food', 'Travel', 'Shop', 'Health', 'Bank', 'Other'])
    .required('Required'),
  type: Yup.string()
    .oneOf(['SPENDING', 'INCOME'])
    .required('Required'),
  value: moreThanZeroValidator,
});

const date = moment();

const AddExpenseForm = () => {
  const modalContext = React.useContext(ModalContext);
  const expenseContext = React.useContext(ExpenseContext);

  return (
    <Modal>
      <FormContainer>
        <Text color="black" size="large" style={{marginBottom: '16px'}}>
          Add New Record
        </Text>
        <Formik
          initialValues={{
            day: date.date(),
            month: date.month() + 1,
            year: date.year(),
            title: '',
            category: '',
            type: '',
            value: 0,
          }}
          validationSchema={AddExpenseSchema}
          onSubmit={values => {
            // same shape as initial values
            // console.log(values);
            const {day, month, year, ...val} = values;
            expenseContext.addExpense({
              date: {
                day,
                month: month - 1,
                year,
              },
              ...val,
            });
            modalContext.toggleModal();
          }}>
          {({resetForm}) => (
            <Form>
              <DateContainer>
                <DateContentContainer>
                  <Label>Day</Label>
                  <Field name="day" component={Input} />
                  <ErrorMessage component={ErrorMessageStyled} name="day" />
                </DateContentContainer>
                <DateContentContainer style={{margin: '0 4px'}}>
                  <Label>Month</Label>
                  <Field name="month" component={Input} />
                  <ErrorMessage component={ErrorMessageStyled} name="month" />
                </DateContentContainer>
                <DateContentContainer>
                  <Label>Year</Label>
                  <Field name="year" component={Input} />
                  <ErrorMessage component={ErrorMessageStyled} name="year" />
                </DateContentContainer>
              </DateContainer>
              <Label>Title</Label>
              <Field name="title" component={Input} />
              <ErrorMessage component={ErrorMessageStyled} name="title" />
              <Label>Category</Label>
              <Field
                name="category"
                component={SelectField}
                options={[
                  {value: 'Food', label: 'Food'},
                  {value: 'Travel', label: 'Travel'},
                  {value: 'Shop', label: 'Shop'},
                  {value: 'Health', label: 'Health'},
                  {value: 'Bank', label: 'Bank'},
                  {value: 'Other', label: 'Other'},
                ]}
              />
              <ErrorMessage component={ErrorMessageStyled} name="category" />
              <Label>Type</Label>
              <Field
                name="type"
                component={SelectField}
                options={[
                  {value: 'SPENDING', label: 'SPENDING'},
                  {value: 'INCOME', label: 'INCOME'},
                ]}
              />
              <ErrorMessage component={ErrorMessageStyled} name="type" />
              <Label>Value</Label>
              <Field name="value" component={Input} />
              <ErrorMessage component={ErrorMessageStyled} name="value" />
              <ButtonContainer>
                <Button fluid type="submit" style={{marginRight: '8px'}}>
                  Submit
                </Button>
                <Button
                  fluid
                  type="cancel"
                  style={{color: 'white'}}
                  onClick={() => {
                    modalContext.toggleModal();
                    resetForm();
                  }}>
                  cancel
                </Button>
              </ButtonContainer>
            </Form>
          )}
        </Formik>
      </FormContainer>
    </Modal>
  );
};

export default AddExpenseForm;
