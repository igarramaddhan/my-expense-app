// @flow
import React from 'react';
import styled, {keyframes} from 'styled-components';
import {ModalContext} from '../contexts/ModalContext';

const fadeIn = keyframes`
  from {
    opacity: 0;
  }

  to {
    opacity: 1;
  }
`;

const fadeOut = keyframes`
  from {
    opacity: 1;
  }

  to {
    opacity: 0;
  }
`;

const ModalContainer = styled.div`
  top: 0;
  left: 0;
  right: 0;
  bottom: 0;
  z-index: 10;
  display: flex;
  position: fixed;
  align-self: center;
  flex-direction: column;
  background-color: rgba(0, 0, 0, 0.5);
  visibility: ${props => (props.active ? 'visible' : 'hidden')};
  animation: ${props => (props.active ? fadeIn : fadeOut)} 200ms linear;
`;

const ModalContent = styled.div`
  width: 100%;
  height: 100%;
  display: flex;
  max-width: 480px;
  align-self: center;
  flex-direction: column;
`;

type Props = {
  children: any,
};

const Modal = (props: Props) => {
  const ctx = React.useContext(ModalContext);
  return (
    <ModalContainer active={ctx.modalIsActive}>
      <ModalContent>{props.children}</ModalContent>
    </ModalContainer>
  );
};

export default Modal;
