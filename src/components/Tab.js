// @flow
import React from 'react';
import styled from 'styled-components';

const TabContainer = styled.div`
  height: 32px;
  display: flex;
  margin: 8px 0;
  align-self: center;
  border-radius: 16px;
  align-items: center;
  justify-content: center;
  width: calc(100% - 30px);
  background-color: ${props => props.backgroundColor};
`;

const TabItem = styled.div`
  flex: 1;
  height: 100%;
  display: flex;
  cursor: pointer;
  border-radius: 16px;
  align-items: center;
  justify-content: center;
  color: ${props =>
    props.active ? props.activeTextColor : props.inactiveTextColor};
  background-color: ${props =>
    props.active ? props.activeColor : props.inactiveColor};
  box-shadow: ${props =>
    props.active
      ? '0 3px 6px rgba(0, 0, 0, 0.15), 0 3px 6px rgba(0, 0, 0, 0.15)'
      : 'none'};
  z-index: ${props => (props.active ? 11 : 0)};
`;

type Props = {
  active: number,
  items: string[],
  backgroundColor: string,
  itemOptions: {
    activeColor: string,
    inactiveColor: string,
    activeTextColor: string,
    inactiveTextColor: string,
  },
  onItemClick: (index: number) => void,
};

const Tab = (props: Props) => (
  <TabContainer backgroundColor={props.backgroundColor}>
    {props.items.map((val, idx) => (
      <TabItem
        key={idx}
        active={props.active === idx}
        {...props.itemOptions}
        onClick={() => props.onItemClick(idx)}>
        {val}
      </TabItem>
    ))}
  </TabContainer>
);

export default Tab;
