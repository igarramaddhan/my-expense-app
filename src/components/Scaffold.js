// @flow
import React from 'react';
import styled from 'styled-components';
import COLORS from '../lib/colors';
import Header from './Header';
import {getValueFromObject} from '../lib/helper';

const ScaffoldContainer = styled.div`
  width: 100%;
  display: flex;
  max-width: 480px;
  padding-top: ${props => `${props.paddingTop}px`};
  position: relative;
  align-self: center;
  flex-direction: column;
  min-height: calc(100vh - 56px);
  background-color: ${COLORS.BACKGROUND};
`;

type Props = {
  children: any,
  headerText?: string,
  headerColor?: string,
  headerTextColor?: string,
  extraHeaderContent?: any,
};

const headerRef = React.createRef<HTMLElement>();

const Scaffold = (props: Props) => {
  const [headerHeight, setHeaderHeight] = React.useState(56);
  React.useEffect(() => {
    const height = getValueFromObject(headerRef, 'current.clientHeight');
    if (height !== headerHeight) {
      setHeaderHeight(height);
    }
  }, []);
  return (
    <ScaffoldContainer paddingTop={headerHeight}>
      <Header
        ref={headerRef}
        text={props.headerText}
        textColor={props.headerTextColor}
        backgroundColor={props.headerColor}>
        {props.extraHeaderContent}
      </Header>
      {props.children}
    </ScaffoldContainer>
  );
};

export default Scaffold;
