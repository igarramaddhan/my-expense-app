// @flow
import uuid from 'uuid/v4';
import moment from 'moment';
import {type Expense} from '../contexts/ExpenseContext';

const createDateFrom = (day, month, year) =>
  moment()
    .date(day)
    .month(month)
    .year(year);

export const expenses: Expense[] = [
  {
    id: uuid(),
    date: createDateFrom(10, 1, 2019),
    title: 'Hello 1',
    category: 'Food',
    type: 'SPENDING',
    value: 20000,
  },
  {
    id: uuid(),
    date: createDateFrom(11, 2, 2019),
    title: 'Hello 2',
    category: 'Travel',
    type: 'SPENDING',
    value: 30000,
  },
  {
    id: uuid(),
    date: createDateFrom(3, 3, 2020),
    title: 'Hello 3',
    category: 'Shop',
    type: 'INCOME',
    value: 20000,
  },
  {
    id: uuid(),
    date: createDateFrom(5, 1, 2019),
    title: 'Hello 4',
    category: 'Health',
    type: 'INCOME',
    value: 50000,
  },
  {
    id: uuid(),
    date: createDateFrom(2, 2, 2019),
    title: 'Hello 5',
    category: 'Food',
    type: 'SPENDING',
    value: 5000,
  },
  {
    id: uuid(),
    date: createDateFrom(3, 4, 2020),
    title: 'Hello 3',
    category: 'Food',
    type: 'INCOME',
    value: 40000,
  },
];
