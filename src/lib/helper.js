export function isDarkColor(color) {
  // Variables for red, green, blue values
  let r = null,
    g = null,
    b = null,
    hsp = null;

  // Check the format of the color, HEX or RGB?
  if (color.match(/^rgb/)) {
    // If HEX --> store the red, green, blue values in separate variables
    color = color.match(
      /^rgba?\((\d+),\s*(\d+),\s*(\d+)(?:,\s*(\d+(?:\.\d+)?))?\)$/
    );

    r = color[1];
    g = color[2];
    b = color[3];
  } else {
    // If RGB --> Convert it to HEX: http://gist.github.com/983661
    color = +`0x${color.slice(1).replace(color.length < 5 && /./g, '$&$&')}`;

    r = color >> 16;
    g = (color >> 8) & 255;
    b = color & 255;
  }

  // HSP (Highly Sensitive Poo) equation from http://alienryderflex.com/hsp.html
  const hspRed = 0.299 * (r * r);
  const hspGreen = 0.587 * (g * g);
  const hspBlue = 0.114 * (b * b);
  hsp = Math.sqrt(hspRed + hspGreen + hspBlue);

  // Using the HSP value, determine whether the color is light or dark
  return hsp <= 127.5;
}

export function getValueFromObject(obj, key) {
  return key.split('.').reduce(function(o, x) {
    return typeof o == 'undefined' || o === null ? o : o[x];
  }, obj);
}

export function getDateFormatString(str) {
  const dateFormatString = {
    DAY: 'DD-MM-YYYY',
    MONTH: 'MM-YYYY',
    YEAR: 'MM-YYYY',
  };

  return dateFormatString[str];
}

export function convertToRupiah(value) {
  let rupiah = '';
  const valuerev = value
    .toString()
    .split('')
    .reverse()
    .join('');
  for (let i = 0; i < valuerev.length; i++)
    if (i % 3 === 0) rupiah += `${valuerev.substr(i, 3)}.`;
  return `Rp. ${rupiah
    .split('', rupiah.length - 1)
    .reverse()
    .join('')}`;
}
