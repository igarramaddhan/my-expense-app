// @flow
import React from 'react';
import uuid from 'uuid/v4';
import moment from 'moment';
import _ from 'underscore';
import {getDateFormatString} from '../lib/helper';

export type ExpenseCategory =
  | 'Food'
  | 'Travel'
  | 'Shop'
  | 'Health'
  | 'Other'
  | 'Bank';
type ExpensesFilter = 'DAY' | 'MONTH' | 'YEAR';
type ExpenseAddType = {
  date: {
    day: number,
    month: number,
    year: number,
  },
  title: string,
  category: ExpenseCategory,
  type: 'SPENDING' | 'INCOME',
  value: number,
};

export type Expense = {
  id: string,
  date: moment.Moment,
  title: string,
  category: ExpenseCategory,
  type: 'SPENDING' | 'INCOME',
  value: number,
};

export type ExpenseContextType = {
  getExpenses: (
    filter: ExpensesFilter
  ) => {
    day: string,
    expenses: Expense[],
  }[],
  populate: (expenses: Expense[]) => void,
  addExpense: (param: ExpenseAddType) => void,
  deleteExpense: (id: string) => void,
};

export const ExpenseContext = React.createContext<ExpenseContextType>({
  // eslint-disable-next-line no-unused-vars
  getExpenses: (filter: ExpensesFilter) => [],
  // eslint-disable-next-line no-unused-vars
  addExpense: (param: ExpenseAddType) => {},
  // eslint-disable-next-line no-unused-vars
  populate: (expenses: Expense[]) => {},
  // eslint-disable-next-line no-unused-vars
  deleteExpense: (id: string) => {},
});

type ProviderStateType = {
  expenses: Expense[],
};

const group = (expenses: Expense[], type: ExpensesFilter) => {
  const filter = getDateFormatString(type);
  const sortedExpenses = [...expenses].sort((a, b) => a.date - b.date);

  const groups = _.groupBy(sortedExpenses, function(obj) {
    return moment(obj.date.format('DD-MM-YYYY'), 'DD-MM-YYYY')
      .startOf(type.toLowerCase())
      .format(filter);
  });

  const result = _.map(groups, function(group, day) {
    return {
      day,
      expenses: group.reverse(),
    };
  });
  return result.sort(function(a, b) {
    const dateA = moment(a.day, filter),
      dateB = moment(b.day, filter);
    return dateB - dateA;
  });
};

export class ExpenseProvider extends React.Component<any, ProviderStateType> {
  state = {
    expenses: [],
  };

  addExpense = (expense: ExpenseAddType) => {
    const {
      date: {day, month, year},
      ...exp
    } = expense;

    const momentDate = moment()
      .date(day)
      .month(month)
      .year(year);

    const item: Expense = {
      id: uuid(),
      date: momentDate,
      ...exp,
    };
    this.setState(
      prev => ({expenses: [...prev.expenses, item]}),
      () =>
        localStorage.setItem('expenses', JSON.stringify(this.state.expenses))
    );
  };

  getExpenses = (filter?: ExpensesFilter) => {
    let result: {
      day: string,
      expenses: Expense[],
    }[] = [];
    const exp = [...this.state.expenses];
    switch (filter) {
      case 'DAY':
        result = group(exp, 'DAY');
        break;
      case 'MONTH':
        result = group(exp, 'MONTH');
        break;
      case 'YEAR':
        result = group(exp, 'YEAR');
        break;
    }

    // console.log(result);

    return result;
  };

  deleteExpense = (id: string) => {
    const temp = this.state.expenses.filter(expense => expense.id !== id);
    this.setState({expenses: temp}, () =>
      localStorage.setItem('expenses', JSON.stringify(temp))
    );
  };

  populate = (expenses: Expense[]) => {
    if (expenses.length !== 0 && moment.isMoment(expenses[0].date)) {
      this.setState({expenses});
    } else {
      const temp = expenses.map(val => ({...val, date: moment(val.date)}));
      this.setState({expenses: temp});
    }
  };

  render() {
    return (
      <ExpenseContext.Provider
        value={{
          populate: this.populate,
          addExpense: this.addExpense,
          getExpenses: this.getExpenses,
          deleteExpense: this.deleteExpense,
        }}>
        {this.props.children}
      </ExpenseContext.Provider>
    );
  }
}

// $FlowFixMe
export function withExpenses<P>(WrappedComponent: React.ComponentType<P>) {
  class EnhancedComponent extends React.Component<P> {
    render() {
      return (
        <ExpenseContext.Consumer>
          {ctx => <WrappedComponent {...this.props} {...ctx} />}
        </ExpenseContext.Consumer>
      );
    }
  }
  return EnhancedComponent;
}
