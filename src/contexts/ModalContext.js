// @flow
import React from 'react';

type ModalContextType = {
  modalIsActive: boolean,
  toggleModal: () => void,
};

export const ModalContext = React.createContext<ModalContextType>({
  modalIsActive: false,
  toggleModal: () => {},
});

type ModalProviderState = {
  modalIsActive: boolean,
};

export class ModalProvider extends React.Component<any, ModalProviderState> {
  state = {
    modalIsActive: false,
  };

  toggleModal = () => {
    if (this.state.modalIsActive) {
      document.removeEventListener('keydown', this.hideModal, false);
    } else {
      document.addEventListener('keydown', this.hideModal, false);
    }
    this.setState(prev => ({modalIsActive: !prev.modalIsActive}));
  };

  hideModal = (event: KeyboardEvent) => {
    if (event.keyCode === 27 && this.state.modalIsActive) {
      this.setState({modalIsActive: false});
    }
  };

  render() {
    return (
      <ModalContext.Provider
        value={{
          modalIsActive: this.state.modalIsActive,
          toggleModal: this.toggleModal,
        }}>
        {this.props.children}
      </ModalContext.Provider>
    );
  }
}
