import React from 'react';
import ReactDOM from 'react-dom';
import App from './App';
import './style/main.scss';
import {ExpenseProvider} from './contexts/ExpenseContext';
import {ModalProvider} from './contexts/ModalContext';

ReactDOM.render(
  <ModalProvider>
    <ExpenseProvider>
      <App />
    </ExpenseProvider>
  </ModalProvider>,
  document.getElementById('root')
);
