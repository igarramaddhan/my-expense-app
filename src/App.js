// @flow
import React from 'react';
import {hot} from 'react-hot-loader/root';
import {BrowserRouter, Route, Switch} from 'react-router-dom';

// import pages here
import Index from './pages/index.js';
import {ExpenseContext} from './contexts/ExpenseContext.js';
import {getValueFromObject} from './lib/helper.js';

function App() {
  const ctx = React.useContext(ExpenseContext);

  const getExpensesFromLocalStorage = () => {
    const raw = localStorage.getItem('expenses');
    if (raw) {
      const localExpenses = JSON.parse(raw);
      getValueFromObject(ctx, 'populate')(localExpenses);
    }
  };

  React.useEffect(() => {
    getExpensesFromLocalStorage();
  }, []);

  return (
    <BrowserRouter>
      <Switch>
        <Route exact path="/" render={props => <Index {...props} />} />
      </Switch>
    </BrowserRouter>
  );
}

export default hot(App);
